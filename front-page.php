<?php 
/*----------------------------------------------------------------*\

	FRONT PAGE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php if ( get_field('slides') ) : ?>
				<section class="image-slider is-standard">
					<?php while( have_rows('slides') ) : the_row(); ?>
						<div class="slide">
							<?php $image = get_sub_field('image'); ?>
							<img src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						</div>
					<?php endwhile; ?>
				</section>
			<?php endif; ?>
			<?php 
				/*----------------------------------------------------------------*\
				|
				| Insert page content which is most often handled via ACF Pro
				| and highly recommend the use of the flexiable content so
				|	we already placed that code here.
				|
				| https://www.advancedcustomfields.com/resources/flexible-content/
				|
				\*----------------------------------------------------------------*/
			?>
			<?php get_template_part('template-parts/article'); ?>
			<?php if ( !empty( get_the_content() ) ) : ?>
				<section class="is-standard">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>