<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php
	// get default filter
	$filterDefault = $_GET["filter"];
	if ( is_null($filterDefault) ) :
		$filterDefault = 'Birthdays';
	endif;
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php the_field('package_title','options'); ?></h1>
	<?php if ( get_field('package_subheading','options') ) : ?>
		<p class="subheading"><?php the_field('package_subheading','options'); ?></p>
	<?php endif; ?>
	<?php if ( get_field('package_featured_image', 'options') ) : ?>
		<?php $image = get_field('package_featured_image', 'options'); ?>
		<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php if ( get_field('package_introduction', 'options') ) : ?>
			<section id="section-1" class="editor is-narrow">
				<p><?php the_field('package_introduction', 'options'); ?></p>
			</section>
		<?php endif; ?>

		<section id="section-2" class="filter">
			<p class="instruction">Swipe to view all party package options</p>
			<div class="package-filters-select">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $keyword = explode(' ',trim( get_field('short_name') )); //first word for class filter ?>
					<button data-filter=".<?php echo $keyword[0]; ?>" class="<?php if ( $keyword[0] == $filterDefault ) : ?>is-checked<?php endif; ?>">
						<?php $image = get_field('icon'); ?>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php the_field('short_name'); ?>
					</button>
				<?php endwhile; ?>
			</div>
		</section>

		<section id="section-2">
			<div class="package-isotope-grid isotope-grid" data-isotope='{ "itemSelector": ".isotope-card", "filter": ".<?php echo $filterDefault; ?>", "layoutMode": "fitRows" }'>
				<?php	$i = 0; while ( have_posts() ) : the_post(); ?>
					<?php $keyword = explode(' ',trim( get_field('short_name') )); //first word for class filter ?>
					<div class="isotope-card <?php echo $keyword[0]; ?>">
						<div class="is-narrow">
							<h2><?php the_title(); ?></h2>
							<?php the_field('introduction'); ?>
						</div>
						<section class="cards is-default">
							<div class="card-grid columns-3 is-wide">
								<?php while ( have_rows('packages') ) : the_row(); ?>
									<div class="card is-toggle">
										<!-- HEADLINE -->
										<?php if ( get_sub_field('title') ) : ?>
											<h3><?php the_sub_field('title') ?></h3>
										<?php endif; ?>
										<!-- SUBHEADLINE -->
										<?php if ( get_sub_field('pricing') ) : ?>
											<p class="subheading"><?php the_sub_field('pricing') ?></p>
										<?php endif; ?>
										<!-- LIST -->	
										<?php if ( get_sub_field('description') ) : ?>
											<details>
												<summary>Tap for Details</summary>
												<?php the_sub_field('description'); ?>
											</details>
										<?php endif; ?>
										<!-- BUTTON -->
										<div>
											<?php
												$link = get_sub_field('button'); 
												$link_url = $link['url'];
												$link_title = $link['title'];
												$link_target = $link['target'] ? $link['target'] : '_self'; 
												if ( get_sub_field('button') ) : 
											?>
												<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
													<?php echo esc_html($link_title); ?>
												</a>
											<?php endif; ?>
											<!-- BUTTON -->
											<?php
												$link = get_sub_field('button_2'); 
												$link_url = $link['url'];
												$link_title = $link['title'];
												$link_target = $link['target'] ? $link['target'] : '_self'; 
												if ( get_sub_field('button') ) : 
											?>
												<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
													<?php echo esc_html($link_title); ?>
												</a>
											<?php endif; ?>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						</section>
						<div class="is-narrow">
							<?php get_template_part('template-parts/article'); ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</section> 

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>