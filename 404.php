<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1>Oops! This page can't be found.</h1>
</header>

<main id="main-content">
	<article>
		<section class="is-narrow">
			<p>Spot a mistake? <a href="https://pib.local/contact/">Contact us</a> and we'll fix it.</p>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>