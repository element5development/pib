var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		MENU OPEN/CLOSE LOGIC
	\*----------------------------------------------------------------*/
	$('button.pop-nav-switch').click(function () {
		$('nav.pop-nav').toggleClass('is-open');
	});
	//if you click on anything except within the open menu, close the menu
	$(document).click(function (event) {
		if (!$(event.target).closest("nav.pop-nav, button.pop-nav-switch").length) {
			$("nav.pop-nav").removeClass("is-open");
		}
	});
	/*----------------------------------------------------------------*\
		DEALS GRID
	\*----------------------------------------------------------------*/
	if ($('.play-deals').length) {
		var d = new Date();
		var weekday = new Array(7);
		weekday[0] = "Sunday";
		weekday[1] = "Monday";
		weekday[2] = "Tuesday";
		weekday[3] = "Wednesday";
		weekday[4] = "Thursday";
		weekday[5] = "Friday";
		weekday[6] = "Saturday";
		var $currentDay = weekday[d.getDay()];
		console.log($currentDay);
		$('.play-deals .card.' + $currentDay).addClass('is-active');
		// FILTER CHANGE EVENT
		$('.deal-filters-select').on('change', function () {
			var $filter = this.value;
			$('.deals .card').removeClass('is-active');
			$('.deals .card' + $filter).addClass('is-active');
			var filterValue = $('.event-filters-select').value;
			$('.event-isotope-grid').isotope({
				filter: filterValue
			});
		});
	}
	/*----------------------------------------------------------------*\
		IMAGE SLIDER
	\*----------------------------------------------------------------*/
	$('.image-slider').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		arrows: false
	});
	/*----------------------------------------------------------------*\
		EVENT ISOTOPE
	\*----------------------------------------------------------------*/
	if ($('.event-isotope-grid').length) {
		var $monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var d = new Date();
		$currentMonth = $monthNames[d.getMonth()];
		var $grid = $('.event-isotope-grid').isotope({
			itemSelector: '.isotope-card',
			filter: '.' + $currentMonth,
			layoutMode: 'fitRows'
		});
		// NO RESULTS MESSAGE
		if (!$('.event-isotope-grid').data('isotope').filteredItems.length) {
			$('#no-event-message').show();
		} else {
			$('#no-event-message').hide();
		}
		// FILTER CHANGE EVENT
		$('.event-filters-select').on('change', function () {
			var filterValue = this.value;
			$('.event-isotope-grid').isotope({
				filter: filterValue
			});
			if (!$('.event-isotope-grid').data('isotope').filteredItems.length) {
				$('#no-event-message').show();
			} else {
				$('#no-event-message').hide();
			}
		});
	}
	/*----------------------------------------------------------------*\
		PACKAGE ISOTOPE
	\*----------------------------------------------------------------*/
	if ($('.package-isotope-grid').length) {
		// FILTER CHANGE EVENT
		$('.package-filters-select').on('click', 'button', function () {
			var filterValue = $(this).attr("data-filter");
			$('.package-isotope-grid').isotope({
				filter: filterValue
			});
		});
		// change is-checked class on buttons
		$(".package-filters-select").each(function (i, buttonGroup) {
			$(buttonGroup).on("click", "button", function () {
				$(buttonGroup).find(".is-checked").removeClass("is-checked");
				$(this).addClass("is-checked");
			});
		});
	}
	/*----------------------------------------------------------------*\
		EAT ISOTOPE
	\*----------------------------------------------------------------*/
	if ($('.eat-isotope-grid').length) {
		// FILTER CHANGE EVENT
		$('.eat-filters-select').on('click', 'button', function () {
			var filterValue = $(this).attr("data-filter");
			$('.eat-isotope-grid').isotope({
				filter: filterValue
			});
		});
		// change is-checked class on buttons
		$(".eat-filters-select").each(function (i, buttonGroup) {
			$(buttonGroup).on("click", "button", function () {
				$(buttonGroup).find(".is-checked").removeClass("is-checked");
				$(this).addClass("is-checked");
			});
		});
	}
	/*----------------------------------------------------------------*\
		PLAY ISOTOPE
	\*----------------------------------------------------------------*/
	if ($('.play-isotope-grid').length) {
		// FILTER CHANGE EVENT
		$('.play-filters-select').on('click', 'button', function () {
			var filterValue = $(this).attr("data-filter");
			$('.play-isotope-grid').isotope({
				filter: filterValue
			});
		});
		// change is-checked class on buttons
		$(".play-filters-select").each(function (i, buttonGroup) {
			$(buttonGroup).on("click", "button", function () {
				$(buttonGroup).find(".is-checked").removeClass("is-checked");
				$(this).addClass("is-checked");
			});
		});
	}
});