<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php
	// get current date
	$current_month = date('F');
	$today = date('Ymd');
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php the_field('event_title','options'); ?></h1>
</header>

<main id="main-content">
	<article>
		<section id="section-1" class="filter is-narrow">
			<select class="event-filters-select">
				<option value=".January" <?php if ( $filterDefault == 'January') : ?>selected<?php endif; ?>>January</option>
				<option value=".February" <?php if ( $filterDefault == 'February') : ?>selected<?php endif; ?>>February</option>
				<option value=".March" <?php if ( $filterDefault == 'March') : ?>selected<?php endif; ?>>March</option>
				<option value=".April" <?php if ( $filterDefault == 'April') : ?>selected<?php endif; ?>>April</option>
				<option value=".May" <?php if ( $filterDefault == 'May') : ?>selected<?php endif; ?>>May</option>
				<option value=".June" <?php if ( $filterDefault == 'June') : ?>selected<?php endif; ?>>June</option>
				<option value=".July" <?php if ( $filterDefault == 'July') : ?>selected<?php endif; ?>>July</option>
				<option value=".August" <?php if ( $filterDefault == 'August') : ?>selected<?php endif; ?>>August</option>
				<option value=".September" <?php if ( $filterDefault == 'September') : ?>selected<?php endif; ?>>September</option>
				<option value=".October" <?php if ( $filterDefault == 'October') : ?>selected<?php endif; ?>>October</option>
				<option value=".November" <?php if ( $filterDefault == 'November') : ?>selected<?php endif; ?>>November</option>
				<option value=".December" <?php if ( $filterDefault == 'December') : ?>selected<?php endif; ?>>December</option>
			</select>
		</section>
		<section id="section-2" class="cards is-default">
			<div class="event-isotope-grid isotope-grid card-grid columns-3 is-wide">
				<?php 
					$posts = get_posts( array(
						'post_type' => 'event',
						'orderby'   => 'start_date',
						'order'     => 'ASC',
						'meta_query' => array(
								array(
										'key'     => 'end_date',
										'compare' => '>=',
										'value'   => $today,
								),
						),
					));
				?>
				<?php	foreach( $posts as $post ) : ?>
					<?php //DATE MANIPULATION
						$start_date_string = get_field('start_date');
						$start_date = DateTime::createFromFormat('Ymd', $start_date_string);
						$start_month = $start_date->format('F');
						$start_display_month = $start_date->format('l F j');
						$end_date_string = get_field('end_date');
						$end_date = DateTime::createFromFormat('Ymd', $end_date_string);
						$end_month = $end_date->format('F');
						$end_display_month = $end_date->format('l M j');
					?>
					<div class="isotope-card card <?php echo $start_month; ?> <?php echo $end_month; ?>">
						<h3><?php the_title() ?></h3>
						<?php if ( get_field('start_date') ) : ?>
							<p class="subheading"><?php echo $start_display_month; ?> @ <?php the_field('start_time'); ?></p>
						<?php endif; ?>
						<!-- DESCRIPTION -->	
						<?php if ( get_field('description') ) : ?>
							<?php the_field('description'); ?>
						<?php endif; ?>
					</div>
				<?php endforeach ?>
			</div>
			<div id="no-event-message" class="no-results-message">
				<?php the_field('no_events_message', 'options'); ?>
			</div>
		</section>
		<?php if ( get_field('event_article', 'options') ) : ?>
			<section id="section-3" class="editor is-narrow">
				<?php the_field('event_article', 'options'); ?>
			</section>
		<?php endif; ?>

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>