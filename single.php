<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
	<article>
		<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
		?>
		<?php get_template_part('template-parts/article'); ?>
		<?php if ( !empty( get_the_content() ) ) : ?>
			<section class="is-standard">
				<?php the_content(); ?>
			</section>
		<?php endif; ?>
	</article>
	<?php else : ?>
	<article>
		<section class="is-narrow">
			<h2>Uh Oh. Something is missing.</h2>
			<p>Looks like this page has no content.</p>
		</section>
	</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>