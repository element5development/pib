<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<?php if ( is_front_page() ) : ?>
		<div class="annoucment">
			<p>
				<span><?php the_field('announcement_label', 'option'); ?></span>
				<?php the_field('announcement_message', 'option'); ?>
				<?php $link = get_field('announcement_button', 'option');?>
				<?php if( $link ): ?>
					<?php
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				</p>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<nav>
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/PIB_LOGO.png" alt="Paradise Island Bowling" />
		</a>
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
			<button class="pop-nav-switch">And More</button>
		</div>
	</nav>
</div>
<nav class="pop-nav">
	<button class="pop-nav-switch">Close</button>
	<?php wp_nav_menu(array( 'theme_location' => 'pop_navigation' )); ?>
	<?php if( have_rows('business_hours', 'option') ): ?>
		<h4>Paradise Hours</h4>
		<ul class="hours">
			<?php while( have_rows('business_hours', 'option') ) : the_row(); ?>
				<li><b><?php the_sub_field('label'); ?></b> <?php the_sub_field('hours'); ?></li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
	<h4>Questions</h4>
	<a href="tel:+14122646570" class="button is-blue">(412) 264-6570</a>
</nav>
<?php if ( is_front_page() ) : ?>
	<?php if( have_rows('business_hours', 'option') ): ?>
		<div class="store-hours is-narrow">
			<h3>Paradise Hours</h3>
			<ul>
				<?php while( have_rows('business_hours', 'option') ) : the_row(); ?>
					<li><b><?php the_sub_field('label'); ?></b> <?php the_sub_field('hours'); ?></li>
				<?php endwhile; ?>
			</ul>
		</div>
	<?php endif; ?>
<?php endif; ?>