<?php
	$id = 0;
	while ( have_rows('article') ) : the_row();
		$id++;
		if( get_row_layout() == 'editor' ):
			hm_get_template_part( 'template-parts/sections/article/editor', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == '2editor' ):
			hm_get_template_part( 'template-parts/sections/article/editor-2-column', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == '3editor' ):
			hm_get_template_part( 'template-parts/sections/article/editor-3-column', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'media+text' ):
			hm_get_template_part( 'template-parts/sections/article/media-text', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'cover' ):
			hm_get_template_part( 'template-parts/sections/article/cover', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'gallery' ):
			hm_get_template_part( 'template-parts/sections/article/gallery', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'image_break' ):
			hm_get_template_part( 'template-parts/sections/article/image-break', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'card_grid' ):
			hm_get_template_part( 'template-parts/sections/article/card-grid', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'map' ):
			hm_get_template_part( 'template-parts/sections/article/map', [ 'sectionId' => $id ] );
		endif;
	endwhile;
?>