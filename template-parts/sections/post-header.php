<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<h1 class="is-narrow"><?php the_title(); ?></h1>
</header>