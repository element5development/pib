<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="cover <?php the_sub_field('width'); ?>">
	<div>
		<h2><?php the_sub_field('title'); ?></h2>
		<div><?php the_sub_field('content'); ?></div>
	</div>
</section>
