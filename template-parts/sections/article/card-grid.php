<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<?php
	$columns = get_sub_field('columns');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="cards <?php the_sub_field('card_type'); ?>">
	<h2 class="<?php the_sub_field('width'); ?>"><?php the_sub_field('title'); ?></h2>
	<div class="card-grid columns-<?php echo $columns; ?> <?php the_sub_field('width'); ?>">
		<?php while ( have_rows('cards') ) : the_row(); ?>
			<div class="card <?php if ( get_sub_field('toggle') ): ?>is-toggle<?php endif; ?>">
				<!-- IMAGE -->
				<?php $image = get_sub_field('icon'); ?>
				<?php if ( get_sub_field('icon') ) : ?>
					<figure>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</figure>
				<?php endif; ?>
				<!-- HEADLINE -->
				<?php if ( get_sub_field('title') ) : ?>
					<h3><?php the_sub_field('title') ?></h3>
				<?php endif; ?>
				<!-- SUBHEADLINE -->
				<?php if ( get_sub_field('subheading') ) : ?>
					<p class="subheading"><?php the_sub_field('subheading') ?></p>
				<?php endif; ?>
				<!-- DESCRIPTION -->	
				<?php if ( get_sub_field('description') ) : ?>
					<?php the_sub_field('description'); ?>
				<?php endif; ?>
				<!-- LIST -->	
				<?php if ( get_sub_field('list') ) : ?>
					<?php if ( get_sub_field('toggle') ) : ?>
						<details>
							<summary>Tap for Details</summary>
							<?php the_sub_field('list'); ?>
						</details>
					<?php else : ?>
						<?php the_sub_field('list'); ?>
					<?php endif; ?>
				<?php endif; ?>
				<!-- BUTTON -->
				<div>
					<?php
						$link = get_sub_field('button'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
						if ( get_sub_field('button') ) : 
					?>
						<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
					<!-- BUTTON -->
					<?php
						$link = get_sub_field('button_2'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
						if ( get_sub_field('button_2') ) : 
					?>
						<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</section>