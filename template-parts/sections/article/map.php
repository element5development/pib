<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a google map link and visual

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="map is-full-width">
	<a target="_blank" href="https://www.google.com/maps/place/Paradise+Island+Bowl/@40.5169933,-80.1494656,15z/data=!4m5!3m4!1s0x0:0xaa588f75010b0967!8m2!3d40.5169933!4d-80.1494656" class="button">Open in Maps</a>
	<svg><use xlink:href="#pin"></use></svg> 
</section>