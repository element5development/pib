<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<section class="business-hours">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/PIB_LOGO.png" alt="Paradise Island Bowling" />
	<?php if( have_rows('business_hours', 'option') ): ?>
		<div class="is-narrow">
			<h3>Paradise Hours</h3>
			<ul>
				<?php while( have_rows('business_hours', 'option') ) : the_row(); ?>
					<li><span><?php the_sub_field('label'); ?></span> <?php the_sub_field('hours'); ?></li>
				<?php endwhile; ?>
			</ul>
		</div>
	<?php endif; ?>
</section>
<footer class="post-footer">
	<div class="is-standard">
		<div class="footer-column">
			<h4>Directions</h4>
			<a target="_blank" href="https://www.google.com/maps/place/Paradise+Island+Bowl/@40.5169933,-80.1494656,15z/data=!4m5!3m4!1s0x0:0xaa588f75010b0967!8m2!3d40.5169933!4d-80.1494656">
				7601 Grand Avenue,<br/>
				Neville Island, PA
			</a>
		</div>
		<div class="footer-column">
			<h4>Play</h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_play' )); ?>
		</div>
		<div class="footer-column">
			<h4>Eat</h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_eat' )); ?>
		</div>
		<div class="footer-column">
			<h4>See</h4>
			<?php wp_nav_menu(array( 'theme_location' => 'footer_see' )); ?>
		</div>
		<div class="footer-column">
			<h4>Questions?</h4>
			Phone: <a href="tel:+14122646570">(412) 264-6570</a><br/>
			Fax: (412) 264-7225
			<a href="<?php the_permalink(563); ?>" class="button contact">Contact Us</a>
			<ul class="social">
				<li>
					<a href="https://www.facebook.com/ParadiseIslandBowl" target="_blank">
						<svg><use xlink:href="#facebook"></use></svg> 
					</a>
				</li>
				<!-- <li>
					<a href="https://twitter.com/PIBowlAndBeach" target="_blank">
						<svg><use xlink:href="#twitter"></use></svg> 
					</a>
				</li> -->
				<li>
					<a href="https://www.instagram.com/paradiseislandbowl/" target="_blank">
						<svg><use xlink:href="#instagram"></use></svg> 
					</a>
				</li>
			</ul>
		</div>
	</div>
</footer>
<div class="copyright">
	<p>Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
	<?php wp_nav_menu(array( 'theme_location' => 'legal_navigation' )); ?>
</div>
<div id="element5-credit">
	<a target="_blank" href="https://element5digital.com" rel="nofollow">
		<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
	</a>
</div>