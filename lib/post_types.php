<?php
/*----------------------------------------------------------------*\
	INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Register Custom Post Type Eat
function create_eat_cpt() {
	$labels = array(
		'name' => _x( 'Eat', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Eat', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Eat', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Eat', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Eat Archives', 'textdomain' ),
		'attributes' => __( 'Eat Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Eat:', 'textdomain' ),
		'all_items' => __( 'All Eat', 'textdomain' ),
		'add_new_item' => __( 'Add New Eat', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Eat', 'textdomain' ),
		'edit_item' => __( 'Edit Eat', 'textdomain' ),
		'update_item' => __( 'Update Eat', 'textdomain' ),
		'view_item' => __( 'View Eat', 'textdomain' ),
		'view_items' => __( 'View Eat', 'textdomain' ),
		'search_items' => __( 'Search Eat', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Eat', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Eat', 'textdomain' ),
		'items_list' => __( 'Eat list', 'textdomain' ),
		'items_list_navigation' => __( 'Eat list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Eat list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Eat', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-carrot',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'eat', $args );
}
add_action( 'init', 'create_eat_cpt', 0 );
// Register Custom Post Type Play
function create_play_cpt() {
	$labels = array(
		'name' => _x( 'Play', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Play', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Play', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Play', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Play Archives', 'textdomain' ),
		'attributes' => __( 'Play Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Play:', 'textdomain' ),
		'all_items' => __( 'All Play', 'textdomain' ),
		'add_new_item' => __( 'Add New Play', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Play', 'textdomain' ),
		'edit_item' => __( 'Edit Play', 'textdomain' ),
		'update_item' => __( 'Update Play', 'textdomain' ),
		'view_item' => __( 'View Play', 'textdomain' ),
		'view_items' => __( 'View Play', 'textdomain' ),
		'search_items' => __( 'Search Play', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Play', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Play', 'textdomain' ),
		'items_list' => __( 'Play list', 'textdomain' ),
		'items_list_navigation' => __( 'Play list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Play list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Play', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-palmtree',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'play', $args );
}
add_action( 'init', 'create_play_cpt', 0 );
// Register Custom Post Type Event
function create_event_cpt() {

	$labels = array(
		'name' => _x( 'Events', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Event', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Events', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Event', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Event Archives', 'textdomain' ),
		'attributes' => __( 'Event Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Event:', 'textdomain' ),
		'all_items' => __( 'All Events', 'textdomain' ),
		'add_new_item' => __( 'Add New Event', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Event', 'textdomain' ),
		'edit_item' => __( 'Edit Event', 'textdomain' ),
		'update_item' => __( 'Update Event', 'textdomain' ),
		'view_item' => __( 'View Event', 'textdomain' ),
		'view_items' => __( 'View Events', 'textdomain' ),
		'search_items' => __( 'Search Event', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Event', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event', 'textdomain' ),
		'items_list' => __( 'Events list', 'textdomain' ),
		'items_list_navigation' => __( 'Events list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Events list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Event', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-calendar-alt',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'create_event_cpt', 0 );
// Register Custom Post Type Party Package
function create_partypackage_cpt() {
	$labels = array(
		'name' => _x( 'Party Packages', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Party Package', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Party Packages', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Party Package', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Party Package Archives', 'textdomain' ),
		'attributes' => __( 'Party Package Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Party Package:', 'textdomain' ),
		'all_items' => __( 'All Party Packages', 'textdomain' ),
		'add_new_item' => __( 'Add New Party Package', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Party Package', 'textdomain' ),
		'edit_item' => __( 'Edit Party Package', 'textdomain' ),
		'update_item' => __( 'Update Party Package', 'textdomain' ),
		'view_item' => __( 'View Party Package', 'textdomain' ),
		'view_items' => __( 'View Party Packages', 'textdomain' ),
		'search_items' => __( 'Search Party Package', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Party Package', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Party Package', 'textdomain' ),
		'items_list' => __( 'Party Packages list', 'textdomain' ),
		'items_list_navigation' => __( 'Party Packages list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Party Packages list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Party Package', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-buddicons-groups',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'partypackage', $args );
}
add_action( 'init', 'create_partypackage_cpt', 0 );