<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'pop_navigation' => __( 'Pop Menu' ),
		'legal_navigation' => __( 'Legal Menu' ),
		'footer_eat' => __( 'Footer Eat Menu' ),
		'footer_play' => __( 'Footer Play Menu' ),
		'footer_see' => __( 'Footer See Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );