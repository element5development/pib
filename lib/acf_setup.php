<?php
/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Archives',
		'menu_title'	=> 'Archives',
		'menu_slug' 	=> 'archives-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Announcement',
		'menu_title'	=> 'Announcement',
		'menu_slug' 	=> 'announcement-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Hours',
		'menu_title'	=> 'Hours',
		'menu_slug' 	=> 'hours-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
?>