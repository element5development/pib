<?php
/*----------------------------------------------------------------*\
	ENQUEUE JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.0', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), '1.0', true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), '1.0', true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
	ENABLE HTML 5 SUPPORT
\*----------------------------------------------------------------*/
add_theme_support( 'html5', array( 
	'comment-list', 
	'comment-form', 
	'search-form', 
	'gallery', 
	'caption' 
) );

/*----------------------------------------------------------------*\
	ENABLE FEATURED IMAGES
\*----------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*----------------------------------------------------------------*\
	ENABLE EXCERPTS
\*----------------------------------------------------------------*/
add_post_type_support( 'page', 'excerpt' );

/*----------------------------------------------------------------*\
	ENABLE RSS FEEDS
\*----------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );

/*----------------------------------------------------------------*\
	ENABLE HTML TITLE TAG
\*----------------------------------------------------------------*/
add_theme_support( 'title-tag' );

/*----------------------------------------------------------------*\
	ENABLE SELECTIVE REFRESH FOR WIDGETS
\*----------------------------------------------------------------*/
add_theme_support( 'customize-selective-refresh-widgets' );

/*----------------------------------------------------------------*\
	ENABLE EDITOR STYLES
\*----------------------------------------------------------------*/
add_theme_support('editor-styles');

/*----------------------------------------------------------------*\
	ENABLE DARK UI STYLES
\*----------------------------------------------------------------*/
//add_theme_support( 'dark-editor-style' );

/*----------------------------------------------------------------*\
	REMOVE H2 FROM DEFAULT WORDPRESS PAGINATION
\*----------------------------------------------------------------*/
function clean_pagination() {
	$thePagination = get_the_posts_pagination();
	echo preg_replace('~(<h2\\s(class="screen-reader-text")(.*)[$>])(.*)(</h2>)~ui', '', $thePagination);
} 

/*----------------------------------------------------------------*\
	REMOVE H1 OPTION FROM EDITOR
\*----------------------------------------------------------------*/
function remove_h1_from_heading($args) {
	$args['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Pre=pre';
	return $args;
}
add_filter('tiny_mce_before_init', 'remove_h1_from_heading' );

/*----------------------------------------------------------------*\
 Like get_template_part() put lets you pass args to the template file
 Args are available in the tempalte as $template_args array
 @param string filepart
 @param mixed wp_args style argument list
\*----------------------------------------------------------------*/
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {
	$template_args = wp_parse_args( $template_args );
	$cache_args = wp_parse_args( $cache_args );
	if ( $cache_args ) {
			foreach ( $template_args as $key => $value ) {
					if ( is_scalar( $value ) || is_array( $value ) ) {
							$cache_args[$key] = $value;
					} else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
							$cache_args[$key] = call_user_method( 'get_id', $value );
					}
			}
			if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
					if ( ! empty( $template_args['return'] ) )
							return $cache;
					echo $cache;
					return;
			}
	}
	$file_handle = $file;
	do_action( 'start_operation', 'hm_template_part::' . $file_handle );
	if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
			$file = get_stylesheet_directory() . '/' . $file . '.php';
	elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
			$file = get_template_directory() . '/' . $file . '.php';
	ob_start();
	$return = require( $file );
	$data = ob_get_clean();
	do_action( 'end_operation', 'hm_template_part::' . $file_handle );
	if ( $cache_args ) {
			wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
	}
	if ( ! empty( $template_args['return'] ) )
			if ( $return === false )
					return false;
			else
					return $data;
	echo $data;
}

/*----------------------------------------------------------------*\
	ARCHIVE SEO
\*----------------------------------------------------------------*/
add_filter( 'the_seo_framework_title_from_generation', function( $title, $args ) {
	/** 
	 * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
	 */
	if ( is_post_type_archive( 'eat' ) ) {
		$title = 'Our Menu';
	}
	if ( is_post_type_archive( 'event' ) ) {
		$title = 'Upcoming Events';
	}
	if ( is_post_type_archive( 'partypackage' ) ) {
		$title = 'Party Packages';
	}
	if ( is_post_type_archive( 'play' ) ) {
		$title = 'Attractions';
	}

	return $title;
}, 10, 2 );

add_filter( 'the_seo_framework_generated_description', function( $description, $args ) {
	/** 
	 * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
	 */
	if ( is_post_type_archive( 'eat' ) ) {
		$description = 'We offer munchies, soup, salads, wings, sandwiches, pizza, and dessert. We also have a kids menu for our Lil Surfers.';
	}
	if ( is_post_type_archive( 'event' ) ) {
		$description = 'Whether it’s for a meeting or a private room for your group, Paradise has everything you need for your next meeting!';
	}
	if ( is_post_type_archive( 'partypackage' ) ) {
		$description = 'We offer bookings for parties directly on our bowling or axe throwing lanes, or in our private party rooms.';
	}
	if ( is_post_type_archive( 'play' ) ) {
		$description = 'Paradise Island Bowl features 24 lanes of upscale bowling with sophisticated design and seating.';
	}

	return $description;
}, 10, 2 );