<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php
	// get default filter
	$filterDefault = $_GET["filter"];
	if ( is_null($filterDefault) ) :
		$filterDefault = 'Bowling';
	endif;
	// get current day
	$current_day = date('l');
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php the_field('play_title','options'); ?></h1>
</header>

<main id="main-content">
	<article>
		<?php if ( get_field('play_introduction', 'options') ) : ?>
			<section id="section-1" class="editor is-narrow">
				<p><?php the_field('play_introduction', 'options'); ?></p>
			</section>
		<?php endif; ?>

		<section id="section-2" class="filter">
			<p class="instruction">Swipe to view all attractions</p>
			<div class="play-filters-select">
				<?php	while ( have_posts() ) : the_post(); ?>
					<?php $keyword = explode(' ',trim( get_field('short_name') )); //first word for class filter ?>
					<button data-filter=".<?php echo $keyword[0]; ?>" class="<?php if ( $keyword[0] == $filterDefault ) : ?>is-checked<?php endif; ?>">
						<?php $image = get_field('icon'); ?>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php the_field('short_name'); ?>
					</button>
				<?php endwhile; ?>
			</div>
		</section>

		<section id="section-3">
			<div class="play-isotope-grid isotope-grid" data-isotope='{ "itemSelector": ".isotope-card", "filter": ".<?php echo $filterDefault; ?>", "layoutMode": "fitRows" }'>
				<?php	while ( have_posts() ) : the_post(); ?>
					<?php $keyword = explode(' ',trim( get_field('short_name') )); //first word for class filter ?>
					<div class="isotope-card <?php echo $keyword[0]; ?>">
						<?php if ( get_field('featured_image') ) : ?>
							<div class="play-image is-narrow">
								<?php $image = get_field('featured_image'); ?>
								<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</div>	
						<?php endif; ?>
						<section class="is-narrow">
							<h2><?php the_title(); ?></h2>
							<?php the_field('intro'); ?>
						</section>
						<?php if ( get_field('hours') ) : ?>
							<section class="play-hours is-standard">
								<?php the_field('hours'); ?>
							</section>
						<?php endif; ?>
						<?php get_template_part('template-parts/article'); ?>
						<?php if ( get_field('deals') ) : ?>
							<section class="play-deals">
								<div class="is-narrow">
									<h2>Deals & Specials</h2>
									<select class="deal-filters-select">
										<option value=".Monday" <?php if ( $current_day == 'Monday') : ?>selected<?php endif; ?>>Monday</option>
										<option value=".Tuesday" <?php if ( $current_day == 'Tuesday') : ?>selected<?php endif; ?>>Tuesday</option>
										<option value=".Wednesday" <?php if ( $current_day == 'Wednesday') : ?>selected<?php endif; ?>>Wednesday</option>
										<option value=".Thursday" <?php if ( $current_day == 'Thursday') : ?>selected<?php endif; ?>>Thursday</option>
										<option value=".Friday" <?php if ( $current_day == 'Friday') : ?>selected<?php endif; ?>>Friday</option>
										<option value=".Saturday" <?php if ( $current_day == 'Saturday') : ?>selected<?php endif; ?>>Saturday</option>
										<option value=".Sunday" <?php if ( $current_day == 'Sunday') : ?>selected<?php endif; ?>>Sunday</option>
									</select>
								</div>
								<div class="deals cards is-default">
									<div class="card-grid columns-3 is-wide">
										<?php while ( have_rows('deals') ) : the_row(); ?>
											<?php $days = get_sub_field('availability'); ?>
											<div class="card <?php foreach( $days as $day ): ?><?php echo $day; ?> <?php endforeach; ?>">
												<?php if ( get_sub_field('title') ) : ?>
													<h3><?php the_sub_field('title') ?></h3>
												<?php endif; ?>
												<?php if ( get_sub_field('subheading') ) : ?>
													<p class="subheading"><?php the_sub_field('subheading') ?></p>
												<?php endif; ?>
												<?php if ( get_sub_field('description') ) : ?>
													<?php the_sub_field('description'); ?>
												<?php endif; ?>
											</div>
										<?php endwhile; ?>
										<div class="no-deals">
											<h4>There are no deals for this day.</h4>
										</div>
									</div>
								</div>
							</section>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		</divclass=> 

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>